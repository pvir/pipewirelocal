#!/bin/bash
set -e -o pipefail
export PREFIX="$HOME/.local/lib/pipewire-local/"
rm "$HOME/.config/systemd/user"/pipewire* "$HOME/.config/systemd/user"/wireplumber*
systemctl --user daemon-reload
echo "Do also rm -rf '$PREFIX'"
