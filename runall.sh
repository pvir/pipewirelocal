#!/bin/bash
#
# Stop pipewire systemd services, and run it in console with extra debug logging.
#

systemctl --user stop pipewire.{service,socket} pipewire-pulse.{service,socket} pipewire-media-session.service wireplumber.service

set -e

cd $(dirname "$0")

PROC1=
PROC2=
PROC3=

LOGDIR=/tmp/pwlog

mkdir -p "$LOGDIR"
chmod og= "$LOGDIR"
> "$LOGDIR/pw.log"
> "$LOGDIR/pw-ms.log"
> "$LOGDIR/pw-pulse.log"

export PIPEWIRE_LOG_SYSTEMD=false

WP=1
if [ "$1" = "pms" ]; then
    WP=
    shift;
fi

if [ -n "$PIPEWIRE_DEBUG" ]; then
    DBG="$PIPEWIRE_DEBUG"
else
    DBG=4
fi

cleanup() {
    kill -9 "$PROC1" "$PROC2" "$PROC3" > /dev/null 2>&1
    exit 0
}

trap cleanup ERR EXIT

export XDG_DATA_DIRS=/home/pauli/.local/lib/pipewire-local/share

export ASAN_OPTIONS=leak_check_at_exit=false:print_summary=true:abort_on_error=true:use_madv_dontdump=true:disable_coredump=false:unmap_shadow_on_exit=true

PIPEWIRE_DEBUG=spa.bluez5*:5 /home/pauli/.local/lib/pipewire-local/bin/pipewire 2>&1 | tee -a "$LOGDIR/pw.log" > /dev/null 2>&1 &
PROC1=$!

sleep 0.5

if [ "$WP" == "1" ]; then
    WIREPLUMBER_DEBUG=4 /home/pauli/.local/lib/pipewire-local/bin/wireplumber 2>&1 | tee -a "$LOGDIR/pw-ms.log" > /dev/null 2>&1 & 
    PROC2=$!
else
    PIPEWIRE_DEBUG=5 /home/pauli/.local/lib/pipewire-local/bin/pipewire-media-session 2>&1 | tee -a "$LOGDIR/pw-ms.log" > /dev/null 2>&1 &
    PROC2=$!
fi

PIPEWIRE_DEBUG=4 /home/pauli/.local/lib/pipewire-local/bin/pipewire-pulse 2>&1 | tee -a "$LOGDIR/pw-pulse.log" > /dev/null 2>&1 &
PROC3=$!

#tail -f "$LOGDIR/pw.log" "$LOGDIR/pw-ms.log" "$LOGDIR/pw-pulse.log"

wait
