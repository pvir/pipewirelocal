#!/bin/bash

set -e -o pipefail

print_header() {
    echo ""
    echo "-- $@"
    echo ""
}

run() {
    echo "$@"
    "$@"
}

print_header "Fetching pipewire sources..."

cd "$(dirname "$0")"

source ./config.sh

PREFIX="$HOME/.local/lib/pipewire-local/"
LDFLAGS="-Wl,-rpath=$PREFIX/lib -Wl,-rpath=$PREFIX/lib64"
PKG_CONFIG_PATH="$PREFIX/lib64/pkgconfig${PKG_CONFIG_PATH:+:}${PKG_CONFIG_PATH}"
export CFLAGS LDFLAGS PKG_CONFIG_PATH

if [[ "$ADDRESS_SANITIZER" = "1" ]]; then
    OPTS="$OPTS -Db_sanitize=address,undefined"
fi

if [[ ! -d pipewire ]]; then
    run git clone https://gitlab.freedesktop.org/pipewire/pipewire.git pipewire
fi

pushd pipewire

if [[ "$1" = "" ]]; then
    true;
else
    run git fetch origin
    run git reset --hard
    run git clean -f -d -x

    run git checkout origin/master
    run git branch -D tmp || true
    run git checkout -b tmp "$1"
    shift
    while [[ $# -gt 0 ]]; do
        run git merge -m "merge" "$1"
        shift
    done
fi


if [[ "$ADDRESS_SANITIZER" = "1" ]]; then
    ASAN_OPTIONS="detect_leaks=1:leak_check_at_exit=0:print_summary=1:abort_on_error=1:use_madv_dontdump=1:disable_coredump=0:unmap_shadow_on_exit=1:strict_string_checks=1:detect_stack_use_after_return=1:check_initialization_order=1:strict_init_order=1"
    UBSAN_OPTIONS="print_stacktrace=1:print_summary=1:abort_on_error=1"
    LSAN_OPTIONS="exitcode=0:log_threads=1"

    cat <<EOF > asan-default-flags.c
    const char *__asan_default_options() {  return "$ASAN_OPTIONS"; }
    const char *__ubsan_default_options() {  return "$UBSAN_OPTIONS"; }
    const char *__lsan_default_options() {  return "$LSAN_OPTIONS"; }
EOF
    run gcc -fPIC -c -o asan-default-flags.o asan-default-flags.c
    LDFLAGS="$LDFLAGS -Wl,$PWD/asan-default-flags.o -Wl,-z -Wl,nodelete"
fi

print_header "Building pipewire..."

run ./autogen.sh --prefix="$PREFIX" \
    -Dsystemd-system-unit-dir="$PREFIX/lib/systemd/system" \
    -Dsystemd-user-unit-dir="$PREFIX/lib/systemd/user" \
    -Dudevrulesdir="$PREFIX/lib/udev/rules.d" \
    -Dsession-managers=wireplumber,media-session \
    $OPTS

run meson compile -C builddir --ninja-args="-k 0"

if [[ -d "$PREFIX" ]] && [[ -f "$PREFIX/pipewire-local-installation.stamp" ]]; then
    print_header "Removing $PREFIX..."
    run rm -rf "$PREFIX"
fi

print_header "Installing pipewire to $PREFIX ..."
run mkdir -p "$PREFIX"
run touch "$PREFIX/pipewire-local-installation.stamp"
run meson install -C builddir

print_header "Adding link to binaries to $HOME/.local/bin ..."

run mkdir -p "$HOME/.local/bin"
run ln -sf "$PREFIX"/bin/* "$HOME/.local/bin/"

if [[ "$INSTALL_SERVICES" = "1" ]]; then
    print_header "Copying pipewire*.{service,socket} to $HOME/.config/systemd/user/ ..."
    run mkdir -p "$HOME/.config/systemd/user/"
    if [[ "$ADDRESS_SANITIZER" = "1" ]]; then
        run sed -i -e "/Type=simple/iEnvironment=ASAN_OPTIONS=$ASAN_OPTIONS" "$PREFIX"/lib/systemd/user/*.{service,socket}
        run sed -i -e '/^\(LockPersonality\|MemoryDenyWriteExecute\|NoNewPrivileges\|RestrictNamespaces\|SystemCallArchitectures\|SystemCallFilter\)/s/^/#/;' "$PREFIX"/lib/systemd/user/*.{service,socket}
    fi
    run sed -i -e "/Type=simple/iTimeoutStopSec=2\nEnvironment=XDG_DATA_DIRS=${PREFIX}/share" "$PREFIX"/lib/systemd/user/*.{service,socket}
    run cp -f "$PREFIX"/lib/systemd/user/*.{service,socket} "$HOME/.config/systemd/user/"
    run systemctl --user daemon-reload
fi

if [[ "$WIREPLUMBER" = "1" ]]; then
    SMSRV=wireplumber.service
else
    SMSRV=pipewire-media-session.service
fi

run systemctl --user try-restart pipewire.service $SMSRV pipewire-pulse.service

print_header "Installation done."

echo "Installed pipewire locally to $PREFIX"
echo "You may also need to:"
echo "* Copy the file $PREFIX/lib/udev/rules.d/90-pipewire-alsa.rules to /lib/udev/rules.d/"
echo "  sudo udevadm control --reload"
echo "* If you are currently using Pulseaudio, you need to also disable its"
echo "  systemd service, and enable the Pipewire ones."
