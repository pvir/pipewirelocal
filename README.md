## pipewirelocal

Scripts for quickly building and installing current pipewire master branch to
your home directory.  Does not need root permissions to run or conflict with
system-wide pulseaudio or pipewire installations.

You may need to copy an udev configuration file manually as root,
however.

Script `install-pipewire.sh`:

- It will build and install Pipewire to: `~/.local/lib/pipewire-local/`

- It will install systemd service files to: `~/.config/systemd/user/`
  if `INSTALL_SERVICES=1` in `config.sh`.

Script `uninstall-pipewire.sh`:

- It will remove Pipewire systemd service files from `~/.config/systemd/user/`

Script `runall.sh`:

- This is for stopping Pipewire systemd services, and run it with
  debug logging to files in terminal.

No warranty: it's supposed to work, but if it breaks something you'll
need to fix things up yourself.  So read through the script files
first.
